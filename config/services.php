<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '525078371215581',
        'client_secret' => 'adca33efb08d3e5d9d9f37bb7a8c2848',
        'redirect' => 'http://localhost:8000/api/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '774344595383-orq1qjkufj0kaml32prenrh54dsjljqd.apps.googleusercontent.com',
        'client_secret' => 'pBkV_YbyBGceyzFIshR1lUn0',
        'redirect' => 'http://localhost:8000/api/login/google/callback',
    ],

    'github' => [
        'client_id' => '9ff58baed098cbbee272',
        'client_secret' => '97894173179da6f09529183d555fa35073468fad',
        'redirect' => 'http://localhost:8000/api/login/github/callback',
    ],

];
