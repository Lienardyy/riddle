<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\User;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(190);
        view()->composer('layouts.*', function($view) {
            $yes = 0;
            $yes1 = 0;
            $yes2 = 1;
            $yes3 = 1;
            $yes4 = 1;
            if (Auth::check()) {
                $dept_id = Auth::User()->id;
                $user = User::where('id', '=', $dept_id)->where('department', '=', 'ADMIN')->first();
                $yes = count($user);
                $yes1 = 1;
                $user2 = User::where('id', '=', $dept_id)->whereNotNull('facebook_id')->first();
                $yes2 = count($user2);
                $user3 = User::where('id', '=', $dept_id)->whereNotNull('google_id')->first();
                $yes3 = count($user3);
                $user4 = User::where('id', '=', $dept_id)->whereNotNull('github_id')->first();
                $yes4 = count($user4);
            }
            $view->with('admin', $yes)->with('auth', $yes1)->with('facebook', $yes2)->with('google', $yes3)->with('github', $yes4);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
