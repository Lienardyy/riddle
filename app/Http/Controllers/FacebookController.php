<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;
use App\Mail\verifyEmail;
use App\User;
use Auth;
use Mail;
use Session;

class FacebookController extends Controller
{

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        $userLogin = User::where('facebook_id', '=', $user->getId())->first();
        if (count($userLogin)) {
            if (!Auth::check()) {
                Auth::loginUsingId($userLogin->id);
            } else {
                session::flash('Exist', 'That Account already Exist!');
            }
        } else {
            if (!Auth::check()) {

                $user_data = User::create([
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'password' => bcrypt('1234'),
                    'facebook_id' => $user->getId(),
                    'verifyToken' => Str::random(40),
                ]);
                $user_data2 = User::find($user_data->id);
                $user_data2->facebook_id = $user->getId();
                $user_data2->save();

                // $user_data = new User();
                // $user_data->name = $user->getName();
                // $user_data->email = $user->getEmail();
                // $user_data->password = bcrypt('1234');
                // $user_data->facebook_id = $user->getId();
                // $user_data->save();

                $thisUser = User::findOrFail($user_data->id);
                $this->sendEmail($thisUser);
                return view('email.verifyEmailFirst');
            } else {
                $user_id = Auth::user()->id;
                $user_data = User::find($user_id);
                $user_data->facebook_id = $user->getId();
                $user_data->save();
                session::flash('Success', 'Register Success!');
            }
        }
        return redirect('api/home');
    }

    public function sendEmail($thisUser)
    {
        Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
    }

    public function verifyEmailFirst()
    {
        return view('email.verifyEmailFirst');
    }

    public function sendEmailDone($email, $verifyToken)
    {
        $user = User::where(['email'=>$email,'verifyToken'=>$verifyToken])->first();
        if ($user) { 
            return User::where(['email'=>$email,'verifyToken'=>$verifyToken])->update(['status'=>'1','verifyToken'=>NULL]);
        } else {
            return 'User Not Found';
        }
    }
}
