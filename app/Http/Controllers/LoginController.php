<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{

    public function checkLogin()
    {
        if(Auth::check()) {
            return view('home');
        } else {
            return view('auth\login');
        }
    }

    public function showLogin() {
        return view('login');
    }

    public function doLogin(Request $request) {
        $auth = Auth::attempt([
            'email'  => $request->input('email'),
            'password'  => $request->input('password')    
            ]);
        return redirect('/api');
    }
}
