<?php

namespace App\Http\Controllers;

use App\Riddle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RiddleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $riddle = Riddle::inRandomOrder()->offset(0)->limit(10)->get();
        $riddleList = Riddle::orderBy('answer', 'asc')->get();
        $riddleAnswer = DB::select( DB::raw("SELECT * FROM riddles WHERE answer IN(SELECT answer FROM (SELECT answer, COUNT(*) as asdf FROM `riddles` GROUP BY answer
                                    ORDER BY `riddles`.`answer` ASC) as test WHERE asdf > 1) ORDER BY answer ASC"));
        $response = ['response' => $riddle, 'answer' => $riddleAnswer, 'list' => $riddleList];
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $riddle = new Riddle();
        $riddle->question = $request->input('question');
        $riddle->answer = $request->input('answer');
        $riddle->save();
        return response()->json(['riddle' => $riddle], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $riddle = Riddle::find($id);
        return response()->json(['riddle' => $riddle], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $riddle = Riddle::find($id);
        $riddle->question = $request->input('question');
        $riddle->answer = $request->input('answer');
        $riddle->save();
        return response()->json(['riddle' => $riddle], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $riddle = Riddle::find($id);
        $riddle->delete();
        return response()->json(['riddle' => $riddle], 200);
    }
}
