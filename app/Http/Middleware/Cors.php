<?php

namespace App\Http\Middleware;

use Closure;
use App\Domain;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {

    //      //Here we put our client domains
    //     $trusted_domains = ["http://localhost:4200"];
        
    //     if(isset($request->server()['HTTP_ORIGIN'])) {
    //         $origin = $request->server()['HTTP_ORIGIN'];

    //         if(in_array($origin, $trusted_domains)) {
    //             header('Access-Control-Allow-Origin: ' . $origin);
    //             header('Access-Control-Allow-Headers: Origin, Content-Type');
    //         }
    //     }

    //     return $next($request);
    // }
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Credentials', '*')
            ->header('Access-Control-Allow-Methods', '*')
            ->header('Access-Control-Allow-Headers', '*');
    }
}