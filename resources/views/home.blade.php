@extends('layouts.app')

@section('content')
<div class="container">
    @if(Session::has("Exist"))
    <div class="alert alert-danger">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("Exist") !!}
    </div>
    @elseif(Session::has("Success"))
    <div class="alert alert-success">
      <span class="glyphicon glyphicon-ok-sign"></span>
      {!! Session("Success") !!}
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
