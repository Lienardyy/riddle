<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', 'auth\LoginController@checkLogin');

//Socialite Facebook
Route::get('login/facebook', 'FacebookController@redirectToProvider')->name('login.facebook');
Route::get('login/facebook/callback', 'FacebookController@handleProviderCallback')->name('login.facebook.callback');
Route::get('register_facebook', 'FacebookController@redirectToProvider')->name('register_facebook');

//Socialite Google
Route::get('login/google', 'GoogleController@redirectToProvider')->name('login.google');
Route::get('login/google/callback', 'GoogleController@handleProviderCallback')->name('login.google.callback');
Route::get('register_google', 'GoogleController@redirectToProvider')->name('register_google');

//Socialite Github
Route::get('login/github', 'GithubController@redirectToProvider')->name('login.github');
Route::get('login/github/callback', 'GithubController@handleProviderCallback')->name('login.github.callback');
Route::get('register_github', 'GithubController@redirectToProvider')->name('register_github');

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	//Socialite Facebook
	Route::get('register_facebook', 'FacebookController@redirectToProvider')->name('register_facebook');

	//Socialite Google
	Route::get('register_google', 'GoogleController@redirectToProvider')->name('register_google');

	//Socialite Github
	Route::get('register_github', 'GithubController@redirectToProvider')->name('register_github');

	Route::get('/home', 'HomeController@index')->name('home');
});
*/