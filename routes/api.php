<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

Route::get('/', 'LoginController@checkLogin');
Route::get('/login', 'LoginController@showLogin')->name('login');
Route::post('/login', 'LoginController@doLogin');*/

// Route::get('/', 'auth\LoginController@checkLogin');
// Route::get('/users', 'HomeController@userShow');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function() {
	Route::resource('users', 'UserController');
	Route::resource('riddles', 'RiddleController');
	Route::resource('scores', 'ScoreController');
});

Route::resource('register2', 'RegisterController');
Route::resource('social', 'SocialController', ['only' => ['store', 'update']]);

Route::get('verify/{email}/{verifyToken}', 'RegisterController@sendEmailDone')->name('sendEmailDone');
// Route::get('verifyEmailFirst', 'RegisterController@verifyEmailFirst')->name('verifyEmailFirst');

Auth::routes();


// Route::post('register2', 'old\RegisterController@create');

//Socialite Facebook
// Route::get('login/facebook', 'FacebookController@redirectToProvider')->name('api.login.facebook');
// Route::get('login/facebook/callback', 'FacebookController@handleProviderCallback')->name('api.login.facebook.callback');
// Route::get('register_facebook', 'FacebookController@redirectToProvider')->name('register_facebook');

// //Socialite Google
// Route::get('login/google', 'GoogleController@redirectToProvider')->name('api.login.google');
// Route::get('login/google/callback', 'GoogleController@handleProviderCallback')->name('api.login.google.callback');
// Route::get('register_google', 'GoogleController@redirectToProvider')->name('register_google');

// //Socialite Github
// Route::get('login/github', 'GithubController@redirectToProvider')->name('api.login.github');
// Route::get('login/github/callback', 'GithubController@handleProviderCallback')->name('api.login.github.callback');
// Route::get('register_github', 'GithubController@redirectToProvider')->name('register_github');

// Route::group(['middleware' => 'auth'], function() {
// 	//Socialite Facebook
// 	// Route::get('register_facebook', 'FacebookController@redirectToProvider')->name('register_facebook');

// 	// //Socialite Google
// 	// Route::get('register_google', 'GoogleController@redirectToProvider')->name('register_google');

// 	// //Socialite Github
// 	// Route::get('register_github', 'GithubController@redirectToProvider')->name('register_github');

// 	Route::get('/home', 'HomeController@index')->name('home');
// });
